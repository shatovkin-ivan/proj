const {Schema, model, Types} = require('mongoose')

const schema = new Schema({
    description: { type: String, required: true },
    link: { type: String, required: true },
    priority: { type: String, required: true },
    isReady: { type: Boolean, required: true },
    comments: { type: Array, required: true },

    links: [{ type: Types.ObjectId, ref: 'Link' }]
})

module.exports = model('Note', schema) 