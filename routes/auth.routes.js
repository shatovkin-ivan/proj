const { Router } = require('express')
const jwt = require('jsonwebtoken')
const { check, validationResult } = require('express-validator')
const bcrypt = require('bcryptjs')
const User = require('../models/User')
const Note = require('../models/Note')
const router = Router()
const config = require('config')

router.post('/register', 
    [
        check('email', 'Uncorrect email').isEmail(),
        check('password', 'Min length - 6').isLength({ min: 6 })
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'please enter a valid email and password'
                })
            }
            const { email, password } = req.body
            const candidate = await User.findOne({ email })
            if (candidate) {
                return res.status(400).json('user already exists')
            }
            const hashedPassword = await bcrypt.hash(password, 12)
            const user = User({ email, password: hashedPassword })
            await user.save()
            res.status(201).json({ message: 'User succesfully created' })
        } catch(e) {
            res.status(500).json({message: 'Error. Something wrong'})
        }
    }
)

router.post('/login', 
    [
        check('email', 'enter correct email').normalizeEmail().isEmail(),
        check('password', 'enter a valid password. min length 6').exists()
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array(),
                message: 'Uncorrect data'
            })
        }
        const { email, password } = req.body
        const user = await User.findOne({ email })
        if (!user) {
            return res.status(400).json({ message: 'user not found' })
        }
        const isMatch = await bcrypt.compare(password, user.password)
        if (!isMatch) {
            return res.status(400).json({ message: 'wrong password, please try again' })
        }
        const token = jwt.sign(
            { userId: user.id },
            config.get('jwtSecret'),
            { expiresIn: '1h' }
        )
        res.json({ token, userId: user.id })
        } catch(e) {
            res.status(500).json({message: 'Error. Something wrong'})
        }
    }
)

router.post('/add-note', 
    async (req, res) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Uncorrect data'
                })
            }
            const { description, link, priority, isReady, comments } = req.body
            const note = Note({ description, link, priority, isReady, comments })
            await note.save()
            res.status(201).json({ message: 'Note created' })
        } catch(e) {
            res.status(500).json({message: 'Error. Something wrong'})
        }
    }
)

module.exports = router
