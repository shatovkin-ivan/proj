export default {
    state: {
        user: {
            id: null,
            token: null,
            name: null
        },
        loading: false,
        message: null,
    },
    actions: {
        async request(ctx, params) {
            const { url, method, body } = params
            try {
                ctx.commit('updateLoading', true)
                const response = await fetch(url, {
                    method,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(body),
                })
                const data = await response.json()
                if (!response.ok) {
                    ctx.commit('updateMessage', data.message || data)
                    throw new Error(data.message || data || 'Something wrong')
                }
                ctx.commit('updateLoading', false)
                ctx.commit('updateMessage', data.message || data)
                return data
            } catch(e) {
                ctx.commit('updateLoading', false)
                ctx.commit('updateMessage', e.message)
                throw e
            }
            
        },
        login(ctx, params) {
            const { token, id, name } = params
            ctx.commit('updateUserToken', token)
            ctx.commit('updateUserId', id)
            ctx.commit('updateUserName', name)
            localStorage.setItem('user', JSON.stringify(ctx.state.user))
        },
        logout(ctx) {
            ctx.commit('updateUserToken', null)
            ctx.commit('updateUserId', null)
            ctx.commit('updateUserName', null)
            ctx.commit('updateMessage', null)
            localStorage.removeItem('user')
        },
        isAuth(ctx) {
            if (localStorage.getItem('user')) {
                const data = JSON.parse(localStorage.getItem('user'))
                ctx.commit('updateUserToken', data.token)
                ctx.commit('updateUserId', data.id)
                ctx.commit('updateUserName', data.name)
            }
        },
        closeMessageWindow(ctx) {
            ctx.commit('updateMessage', null)
        },
    },
    mutations: {
        updateUserId(state, data) {
            state.user.id = data
        },
        updateUserToken(state, data) {
            state.user.token = data
        },
        updateUserName(state, data) {
            state.user.name = data
        },
        updateLoading(state, loading) {
            state.loading = loading
        },
        updateMessage(state, message) {
            state.message = message
        },
    },
    getters: {
        getUser(state) {
            return state.user
        }, 
        getUserId(state) {
            return state.user.id
        },
        getUserToken(state) {
            return state.user.token
        },
        getUserName(state) {
            return state.user.name
        },
        getMessage(state) {
            return state.message
        }
    }
}