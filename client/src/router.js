import { createRouter, createWebHistory } from "vue-router"

import IndexPage from './pages/IndexPage.vue'
import HtmlPage from './pages/HtmlPage.vue'
import CssPage from './pages/CssPage.vue'
import JsPage from './pages/JsPage.vue'
import AuthPage from './pages/AuthPage.vue'
import OtherSoursesPage from './pages/OtherSoursesPage.vue'

export default createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', name: 'main', component: IndexPage, alias: '/' },
        { path: '/html', component: HtmlPage },
        { path: '/css', component: CssPage },
        { path: '/js', component: JsPage },
        { path: '/other', component: OtherSoursesPage },
        { path: '/auth', name: 'auth', component: AuthPage },
    ]
})
